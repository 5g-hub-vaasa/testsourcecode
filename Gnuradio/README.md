# Some simple GnuRadio examples

These programs do not use RF hardware yet. 

 - `first.grc`: A very simple test program which just creates a signal and visualize it in Qt-window
 - `soundgen.grc`: A slightly more complex program which generates a sinusoidal signal with noise. There are two knobs which can be used for controlling the frequency and noise level. The resulting signal is shown in spectrum window, and it is also fed into the soundcard and can be listened. You may need to change the audio device name according to your system, or you may completely remove the audiosink.
 - `usrpsample.grc`: Simple test file wich receives RF-data from USRP and displays it in QT FFT widget
 
The USRP can be used apparently by just adding USRP sources and sinks into the project.
